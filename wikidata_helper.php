<?php

/**
 * Auxiliary library functions for CHEG and CHEBOT
 *
 * @author Avsolov
 *
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 */

use \Mediawiki\DataModel\EditInfo;
use \Wikibase\DataModel\Reference;
use \Wikibase\DataModel\ReferenceList;
use \Wikibase\DataModel\SiteLink;
use \Wikibase\DataModel\SiteLinkList;
use \Wikibase\DataModel\Entity\Item;
use \Wikibase\DataModel\Entity\ItemId;
use \Wikibase\DataModel\Entity\PropertyId;
use \Wikibase\DataModel\Entity\EntityIdValue;
use \Wikibase\DataModel\Snak\PropertyValueSnak;
use \Wikibase\DataModel\Snak\PropertySomeValueSnak;
use \Wikibase\DataModel\Snak\SnakList;
use \Wikibase\DataModel\Statement\Statement;
use \Wikibase\DataModel\Statement\StatementList;
use \DataValues\UnDeserializableValue;
use \DataValues\StringValue;

define('A','... some uniq id for keying ...');
$redis = new \Redis();
$redis->connect('tools-redis.svc.eqiad.wmflabs', 6379);
$cache = new \Cache\Adapter\Redis\RedisCachePool($redis);

define('URL_TFO', 'https://ru_monuments.toolforge.org/wikivoyage.php?id=');
define('URL_WDO', 'https://www.wikidata.org/');
define('URL_RWV', 'https://ru.wikivoyage.org/');
define('URL_RWP', 'https://ru.wikipedia.org/');
define('URL_COM', 'https://commons.wikimedia.org/');

define('P_COUNTRY',       'P17');       $P_COUNTRY       = new PropertyId(P_COUNTRY);
define('P_IMAGE',         'P18');       $P_IMAGE         = new PropertyId(P_IMAGE);
define('P_INSTANCE_OF',   'P31');       $P_INSTANCE_OF   = new PropertyId(P_INSTANCE_OF);
define('P_ARCHITECT',     'P84');       $P_ARCHITECT     = new PropertyId(P_ARCHITECT);
define('P_LOC_ATE',       'P131');      $P_LOC_ATE       = new PropertyId(P_LOC_ATE);
define('P_IMPORTED_FROM', 'P143');      $P_IMPORTED_FROM = new PropertyId(P_IMPORTED_FROM);
define('P_STYLE',         'P149');      $P_STYLE         = new PropertyId(P_STYLE);
define('P_CREATOR',       'P170');      $P_CREATOR       = new PropertyId(P_CREATOR);
define('P_DEPICTS',       'P180');      $P_DEPICTS       = new PropertyId(P_DEPICTS);
define('P_STATED_IN',     'P248');      $P_STATED_IN     = new PropertyId(P_STATED_IN);
define('P_LOCATION',      'P276');      $P_LOCATION      = new PropertyId(P_LOCATION);
define('P_PART_OF',       'P361');      $P_PART_OF       = new PropertyId(P_PART_OF);
define('P_COMMONS_CAT',   'P373');      $P_COMMONS_CAT   = new PropertyId(P_COMMONS_CAT);
define('P_OCCUPANT',      'P466');      $P_OCCUPANT      = new PropertyId(P_OCCUPANT);
define('P_COMMEMORATES',  'P547');      $P_COMMEMORATES  = new PropertyId(P_COMMEMORATES);
define('P_INCEPTION',     'P571');      $P_INCEPTION     = new PropertyId(P_INCEPTION);
define('P_ABOLISHED',     'P576');      $P_ABOLISHED     = new PropertyId(P_ABOLISHED);
define('P_START_TIME',    'P580');      $P_START_TIME    = new PropertyId(P_START_TIME);
define('P_END_TIME',      'P582');      $P_END_TIME      = new PropertyId(P_END_TIME);
define('P_COORDINATES',   'P625');      $P_COORDINATES   = new PropertyId(P_COORDINATES);
define('P_OF',            'P642');      $P_OF            = new PropertyId(P_OF);
define('P_STREET',        'P669');      $P_STREET        = new PropertyId(P_STREET);
define('P_STREET_NUMBER', 'P670');      $P_STREET_NUMBER = new PropertyId(P_STREET_NUMBER);
define('P_REF_URL',       'P854');      $P_REF_URL       = new PropertyId(P_REF_URL);
define('P_HERITAGE',      'P1435');     $P_HERITAGE      = new PropertyId(P_HERITAGE);
define('P_TITLE',         'P1476');     $P_TITLE         = new PropertyId(P_TITLE);
define('P_KNID',          'P1483');     $P_KNID          = new PropertyId(P_KNID);
define('P_DIRECTIONS',    'P2795');     $P_DIRECTIONS    = new PropertyId(P_DIRECTIONS);
define('P_WLM_ID',        'P2186');     $P_WLM_ID        = new PropertyId(P_WLM_ID);
define('P_LIST',          'P2817');     $P_LIST          = new PropertyId(P_LIST);
define('P_WM_URL',        'P4656');     $P_WM_URL        = new PropertyId(P_WM_URL);
define('P_EGROKN',        'P5381');     $P_EGROKN        = new PropertyId(P_EGROKN);
define('P_ADDRESS',       'P6375');     $P_ADDRESS       = new PropertyId(P_ADDRESS);
define('P_SOBORY',        'P8316');     $P_SOBORY        = new PropertyId(P_SOBORY);
define('P_TEMPLES',       'P9343');     $P_TEMPLES       = new PropertyId(P_TEMPLES);
define('Q_RUSSIA',        'Q159');      $Q_RUSSIA        = new EntityIdValue(new ItemId(Q_RUSSIA));
define('Q_UKRAINE',       'Q212');      $Q_UKRAINE       = new EntityIdValue(new ItemId(Q_UKRAINE));
define('Q_EGROKN',         'Q7382189');
define('Q_RUS_HERITAGE',   'Q8346700');
define('Q_RUS_WIKIVOYAGE', 'Q17601812');
define('Q_SOBORY',         'Q105343287');
define('Q_TEMPLES',        'Q105956103');

define('Q_CHURCH', 'Q16970');
define('Q_CHAPEL', 'Q1975485');
define('Q_BUILDING', 'Q41176');

$MW = array();

$WD_URLS = array(
    'sitelinks' => '<a href="%s">%s</a>',
    P_COMMONS_CAT => '<a href="'.URL_COM.'wiki/Category:%s">%s</a>',
    P_KNID => '<a href="https://ru_monuments.toolforge.org/wikivoyage.php?id=%s">%s</a>',
    P_EGROKN => '<a href="https://ru_monuments.toolforge.org/get_info.php?id=%s">%s</a>',
    P_SOBORY => '<a href="https://sobory.ru/article/?object=%s">%s</a>',
    P_TEMPLES => '<a href="http://temples.ru/card.php?ID=%s">%s</a>');

$WD_TYPOLOGY = array(
    'architecture' => 'Q2319498',
    'history' => 'Q1081138',
    'monument' => 'Q4989906',
    'archeology' => 'Q839954',
    'settlement' => 'Q3920245');

$WD_PROTECTION = array(
    ''  => Q_RUS_HERITAGE,
    '4' => 'Q105835782',
    'В' => 'Q105835774',
    'Ф' => 'Q23668083',
    'Р' => 'Q105835744',
    'М' => 'Q105835766');

$TIME_PRECISION = array(9=>1, 8=>10, 7=>100, 6=>1000);
$WD_TIME_PRECISION = array(0=>'млрд лет', 1=>'100 млн лет', 2=>'10 млн лет', 3=>'1 млн лет', 
    4=>'100 тыс. лет', 5=>'10 тыс. лет', 6=>'1 тыс. лет', 7=>'100 лет', 8=>'10 лет', 9=>'1 год',
    10=>'1 мес.', 11=>'1 день', 12=>'1 час', 13=>'1 мин.', 14=>'1 сек.');

/*
 * Retrieve WikibaseFactory instance
 *
 * @param $api                  — MediawikiApi instance
 * @return WikibaseFactory instance
 */
function get_wikidata_factory($api = null) {
    global $wbFactory;
    if (isset($wbFactory)) return $wbFactory;
    if ($api == null) throw new Exception('API is not initialized');
    $dataValueClasses = array(
        'unknown' => 'DataValues\UnknownValue',
        'string' => 'DataValues\StringValue',
        'boolean' => 'DataValues\BooleanValue',
        'number' => 'DataValues\NumberValue');
    $wbFactory = new \Wikibase\Api\WikibaseFactory($api,
        new DataValues\Deserializers\DataValueDeserializer($dataValueClasses),
        new DataValues\Serializers\DataValueSerializer());
    return $wbFactory;
}

/*
 * Determine page revision, wikidata id etc.
 *
 * @param $API_URL              — URL of Mediawiki API
 * @param $title                — page title
 * @return stdClass instance with page properties
 */
function get_page_properties($API_URL, $title) {
    global $MW, $AVSBOT;
    if (!isset($MW[$API_URL])) {
        $MW[$API_URL] = new \Mediawiki\Api\MediawikiApi($API_URL);
        $MW[$API_URL]->login(new \Mediawiki\Api\ApiUser($AVSBOT['LOGIN'], $AVSBOT['PASSWORD']));
    }
    $res = $MW[$API_URL]->getRequest(new \Mediawiki\Api\SimpleRequest('parse', array('page'=>$title, 'prop'=>'wikitext|revid|properties')));
    $res = json_decode(json_encode($res, JSON_UNESCAPED_UNICODE));

    if (property_exists($res, 'error')) {
        error_log("get_page_properties('$API_URL', '$title') failed: {$res->error->code}\n");
        return null;
    }
    $props = array(); 
    if (is_array($res->parse->properties)) {
        foreach ($res->parse->properties as $elem) {
            $props[$elem->name] = $elem->{'*'};
        }
        $res->parse->properties = $props;
    }
    return $res->parse;
}

/**
 * Purge page cache
 *
 * @param $API_URL              — URL of Mediawiki API
 * @param $title                — page title
 * @return stdClass instance with the result
 */
function purge_page_cache($API_URL, $title) {
    global $MW, $AVSBOT;
    if (!isset($MW[$API_URL])) {
        $MW[$API_URL] = new \Mediawiki\Api\MediawikiApi($API_URL);
        $MW[$API_URL]->login(new \Mediawiki\Api\ApiUser($AVSBOT['LOGIN'], $AVSBOT['PASSWORD']));
    }
    sleep(1);
    $result = $MW[$API_URL]->postRequest(new \Mediawiki\Api\SimpleRequest('purge', array('titles'=>$title)));
    sleep(1);
    return $result;
}

/*
 * Create new Wikidata entity
 *
 * @param $row                  — row from sql result
 * @param $bot                  — whether set bot flag or not
 * @return ItemId instance of newly created entity
 */
function create_wd_entity($row, $bot = false) {
    global $WD_TYPOLOGY, $WD_PROTECTION;
    $wbFactory = get_wikidata_factory();

    $edit_info = new EditInfo('export from Russian Wikivoyage'.($bot ? ', #[[:voy:ru:User:Avsolov/chebot|CHEBOT]]' : ''), EditInfo::NOTMINOR, $bot, $bot ? 5 : null);
    $wv = json_decode($row['json']);
    $saver = $wbFactory->newRevisionSaver();
    $refSnaks = array();
    $refSnaks[] = new PropertyValueSnak($GLOBALS['P_IMPORTED_FROM'], new EntityIdValue(new ItemId(Q_RUS_WIKIVOYAGE)));
    if (!empty($row['revid']))
        $refSnaks[] = new PropertyValueSnak($GLOBALS['P_WM_URL'], new StringValue(URL_RWV.'?oldid='.$row['revid'].'#'.$row['knid']));
    $refs = new ReferenceList(array(new Reference($refSnaks)));
    $statements = array();
    $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_INSTANCE_OF'], new EntityIdValue(new ItemId($WD_TYPOLOGY[$wv->type]))), null, $refs);
    if (strpos($wv->name, 'ерковь')) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_INSTANCE_OF'], new EntityIdValue(new ItemId(Q_CHURCH))), null, $refs);
    } else if (strpos($wv->name, 'асовня')) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_INSTANCE_OF'], new EntityIdValue(new ItemId(Q_CHAPEL))), null, $refs);
    } else if (preg_match('|\b[Зз]дание\b|u', $wv->name) or preg_match('|\b[Дд]ом\b|u', $wv->name)) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_INSTANCE_OF'], new EntityIdValue(new ItemId(Q_BUILDING))), null, $refs);
    }
    if (!empty($wv->image)) {
        $wv->image = str_replace('_', ' ', $wv->image);
        $img = get_image($wv->image);
        if ($img['img_base_url'] == URL_COM)
            $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_IMAGE'], new StringValue($wv->image)), null, $refs);
    }
    if (!empty($wv->munid)) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_LOC_ATE'], new EntityIdValue(new ItemId($wv->munid))), null, $refs);
    }
    if (!empty($wv->lat) && !empty($wv->long)) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_COORDINATES'], new UnDeserializableValue(
            array('latitude'=>(float)$wv->lat,'longitude'=>(float)$wv->long,'altitude'=>null,'precision'=>($wv->precise=='yes'?0.000001:0.0001),'globe'=>'http://www.wikidata.org/entity/Q2'),
            'globecoordinate', '')), null, $refs);
    }
    $wv_year = @($wv->year+0);
    if ($wv_year>100) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_INCEPTION'], new UnDeserializableValue(
            array('time'=>"+$wv_year-00-00T00:00:00Z", 'timezone'=>0, 'before'=>0, 'after'=>0, 'precision'=>9, 'calendarmodel'=>'http://www.wikidata.org/entity/Q1985727'),
            'time', '')), null, $refs);
    }
    $statements[] = new Statement(
        new PropertyValueSnak($GLOBALS['P_COUNTRY'], $GLOBALS['Q_RUSSIA']),
        ($wv_year>=1991 ? null :
        new SnakList(array(new PropertyValueSnak($GLOBALS['P_START_TIME'], new UnDeserializableValue(
            array('time'=> ((strncmp($row['knid'],"82",2)==0) || (strncmp($row['knid'],"92",2)==0) ? "+2014-03-18T00:00:00Z" : "+1991-12-25T00:00:00Z"),
                'timezone'=>0, 'before'=>0, 'after'=>0, 'precision'=>11, 'calendarmodel'=>'http://www.wikidata.org/entity/Q1985727'),
            'time', ''))))), 
        $refs);
    $knid_ref = null;
    if (!empty($row['pwd'])) {
        $knid_ref = array(new PropertyValueSnak($GLOBALS['P_STATED_IN'], new EntityIdValue(new ItemId('Q'.$row['pwd']))));
        $knid_ref = new ReferenceList(array(new Reference($knid_ref)));
    }
    $official = official_acts($wv);
    if (empty($official) and $row['knid']{2} == '4' and !empty($row['pwd'])) {
        $acts = $knid_ref;
    } else {
        $acts = new ReferenceList(array_map(function ($val) {return new Reference($val);}, $official));
    }
    if ($row['knid']{2} == '4') $wv->protection = '4';
    if (empty($wv->protection) or empty($WD_PROTECTION[$wv->protection])) $wv->protection = '';
    $qua = null;
    if (property_exists($wv, 'dismissed')) {
        $snak = null;
        if (preg_match('|do?c?(\d{2})(\d{2})(\d{4})|', $wv->dismissed, $d)) {
            $snak = new PropertyValueSnak($GLOBALS['P_END_TIME'],
                new UnDeserializableValue(array('time'=>"+$d[3]-$d[2]-$d[1]T00:00:00Z", 'timezone'=>0, 'before'=>0, 'after'=>0, 'precision'=>11, 'calendarmodel'=>'http://www.wikidata.org/entity/Q1985727'), 'time', ''));
        } else {
            $snak = new PropertySomeValueSnak($GLOBALS['P_END_TIME']);
        }
        $qua = new SnakList(array($snak));
    }
    $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_HERITAGE'], new EntityIdValue(new ItemId($WD_PROTECTION[$wv->protection]))), $qua, $acts);
    if (!empty($row['wv_address'])) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_DIRECTIONS'], new UnDeserializableValue(
            array('language'=>'ru','text'=>$row['wv_address']), 'monolingualtext', '')), null, $refs);
    }
    $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_KNID'], new StringValue($wv->knid)), null, $knid_ref);
    if (!empty($row['pwd'])) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_LIST'], new EntityIdValue(new ItemId('Q'.$row['pwd']))), null, null);
    }
    $qua = null;
    if ((strncmp($wv->knid,"82",2)==0) || (strncmp($wv->knid,"92",2)==0)) {
        if (property_exists($wv, 'uid') and !empty($wv->uid)) {
            $statements[] = new Statement(
                new PropertyValueSnak($GLOBALS['P_WLM_ID'], new StringValue('UA-'.$wv->uid)),
                new SnakList(array(new PropertyValueSnak($GLOBALS['P_OF'], $GLOBALS['Q_UKRAINE']))),
                null);
        }
        $qua = new SnakList(array(new PropertyValueSnak($GLOBALS['P_OF'], $GLOBALS['Q_RUSSIA'])));
    }
    $wlmid_refs = array(new PropertyValueSnak($GLOBALS['P_REF_URL'], new StringValue(URL_TFO.$wv->knid)));
    $statements[] = new Statement(
        new PropertyValueSnak($GLOBALS['P_WLM_ID'], new StringValue('RU-'.$wv->knid)), 
        $qua, 
        new ReferenceList(array(new Reference($wlmid_refs))));
    if (!empty($wv->knid_new)) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_EGROKN'], new StringValue($wv->knid_new)), null, 
            new ReferenceList(array(new Reference(array(new PropertyValueSnak($GLOBALS['P_STATED_IN'], new EntityIdValue(new ItemId(Q_EGROKN))))))));
    }
    if (!empty($row['wv_complex_wdid']) && $wv->complex != $wv->knid) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_PART_OF'], new EntityIdValue(new ItemId($row['wv_complex_wdid']))), null, $refs);
    }
    if (!empty($wv->sobory)) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_SOBORY'], new StringValue($wv->sobory)), null, 
            new ReferenceList(array(new Reference(array(new PropertyValueSnak($GLOBALS['P_STATED_IN'], new EntityIdValue(new ItemId(Q_SOBORY))))))));
    }
    if (!empty($wv->temples)) {
        $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_TEMPLES'], new StringValue($wv->temples)), null, 
            new ReferenceList(array(new Reference(array(new PropertyValueSnak($GLOBALS['P_STATED_IN'], new EntityIdValue(new ItemId(Q_TEMPLES))))))));
    }
    if (!empty($wv->status) and $wv->status == 'destroyed') {
        $statements[] = new Statement(new PropertySomeValueSnak($GLOBALS['P_ABOLISHED']), null, $refs);
    }
    $sitelinks = array();
    $use_comcat_as_en_label = false;
    $purge_commonswiki = false;
    if (!empty($wv->commonscat)) {
        $p = get_page_properties(URL_COM.'w/api.php', 'Category:'.$wv->commonscat);
        if (!empty($p)) {
            if (empty($p->properties['wikibase_item'])) {
                if ($bot and empty($row['wv_complex_wdid']) and !empty($wv->complex) and $wv->complex != $wv->knid);
                else {
                    $sitelinks[] = new SiteLink('commonswiki', 'Category:'.$wv->commonscat);
                    $use_comcat_as_en_label = true;
                    $purge_commonswiki = true;
                }
            } else {
                error_log("create_wd_entity(for $row[knid]): commonscat '{$wv->commonscat}' is already bound to {$p->properties['wikibase_item']}");
            }
            $statements[] = new Statement(new PropertyValueSnak($GLOBALS['P_COMMONS_CAT'], new StringValue($wv->commonscat)), null, $refs);
        }
    }
    $purge_ruwiki = false;
    if (!empty($wv->wiki)) {
        $p = get_page_properties(URL_RWP.'w/api.php', $wv->wiki);
        if (!empty($p)) {
            if (empty($p->properties['wikibase_item'])) {
                $sitelinks[] = new SiteLink('ruwiki', $wv->wiki);
                $purge_ruwiki = true;
            } else
                error_log("create_wd_entity(for $row[knid]): wiki '{$wv->wiki}' is already bound to {$p->properties['wikibase_item']}");
        }
    }
    $item = new Item(null, null, new SiteLinkList($sitelinks), new StatementList($statements));
    $label_ru = $wv->name.((mb_strlen($wv->name)<150 and !empty($wv->municipality)) ? " ({$wv->municipality})" : '');
    if (mb_strlen($label_ru)>250) $label_ru = mb_substr($label_ru, 0, 240)."...";
    $item->setLabel('ru', $label_ru);
    if ($use_comcat_as_en_label) {
        $item->setLabel('en', $wv->commonscat);
    }
    $edit = new \Mediawiki\DataModel\Revision(new \Wikibase\DataModel\ItemContent($item));
    $resultingItem = $saver->save($edit, $edit_info);
    $itemId = $resultingItem->getId();
    if ($purge_commonswiki) {
        $res = purge_page_cache(URL_COM.'w/api.php', 'Category:'.$wv->commonscat);
        error_log("... need to purge cache of Category:{$wv->commonscat} ".json_encode($res, JSON_UNESCAPED_UNICODE));
    }
    if ($purge_ruwiki) {
        $res = purge_page_cache(URL_RWP.'w/api.php', $wv->wiki);
        error_log("... need to purge cache of ruwiki:{$wv->wiki} ".json_encode($res, JSON_UNESCAPED_UNICODE));
    }
    return $itemId;
}

/*
 * Substitude wikidata id into monument template of specified heraitage site.
 *
 * @param $page_content         — wikitext of Wikivoyage page
 * @param $id                   — knid (heritage identifier)
 * @param $wdid                 — wikidata entity id
 */
function set_wdid($page_content, $id, $wdid) {
    preg_match('/([{][{]monument[^-][^}]+\|knid\s*=\s*'.$id.'\s*\|[^}]+[}][}])/mu', $page_content, $match);
    $monument = $match[0];
    $monument1 = preg_replace('/\|wdid\s*=\s*[^|]*/', "|wdid= $wdid\n", $monument);
    $new_content = str_replace($monument, $monument1, $page_content);
    return $new_content;
}

/*
 * Check if P17 statement claims Russia (Q159) or else add such statement
 *
 * @param $id                   — knid (heritage identifier)
 * @param $wd                   — statements list of the wikidata entity
 * @param $refSnaks             — references for proposed statement
 * @param $inception            — inception year
 */
function check_and_add_country_statement($id, $wd, $refSnaks, $inception = 0) {
    global $edit_info;
    $need_to_add = true;
    foreach ($wd->getByPropertyId($GLOBALS['P_COUNTRY']) as $stmt) {
        if ($stmt->getMainSnak()->getDataValue()->getValue()['id'] == Q_RUSSIA) {$need_to_add = false; break;}
    }
    if ($need_to_add) {
        $wd->addNewStatement(
            new PropertyValueSnak($GLOBALS['P_COUNTRY'], $GLOBALS['Q_RUSSIA']),
            ($inception>1991 ? null :
            new SnakList(array(new PropertyValueSnak($GLOBALS['P_START_TIME'], new UnDeserializableValue(
                array('time'=> ((strncmp($id,"82",2)==0) || (strncmp($id,"92",2)==0) ? "+2014-03-18T00:00:00Z" : "+1991-12-25T00:00:00Z"),
                    'timezone'=>0, 'before'=>0, 'after'=>0, 'precision'=>11, 'calendarmodel'=>'http://www.wikidata.org/entity/Q1985727'),
                'time', ''))))),
            array(new Reference($refSnaks)));
        $edit_info[] = '[[:d:Property:P17|country (P17)]]: [[:d:Q159|]]';
    }
}

/**
 * Add or replace Wikidata statement
 *
 * @param $wd                   — statements list of the wikidata entity
 * @param $value                — value for PropertyValueSnak instance
 * @param $pid                  — PropertyId of the statement
 * @param $refs                 — array of References
 * @param $keep_qualified       — keep statements with qualifiers
 */
function set_or_replace_value($wd, $value, $pid, $refs, $keep_qualified = false) {
    global $edit_info;
    if (empty($value)) throw new Exception("Значение для $pid не задано");
    $p = $wd->getByPropertyId($pid);
    if (!$p->isEmpty()) foreach ($p as $stmt) {
        if ($keep_qualified) {
            if (!$stmt->getQualifiers()->isEmpty()) continue;
        }
        $wd->removeStatementsWithGuid($stmt->getGuid());
    }
    $wd->addNewStatement(new PropertyValueSnak($pid, $value), null, $refs);
    $edit_info[] = "[[:d:Property:$pid|".get_wd_property($pid.'')." ($pid)]]".
        ($value instanceof EntityIdValue ? sprintf(': [[:d:%s|%s (%s)]]', $value->getEntityId(), get_wd_label($value->getEntityId()), $value->getEntityId())
            : (is_array($value->getValue()) ? '' : ': '.$value->getValue()));
}

/**
 * Render Wikidata statement with main SNAK, qualifiers and references into HTML
 *
 * @param $stmt                 — statement representation as stdClass instance from deserialized json
 * @param $conceptual_value
 * @return text to display in HTML code
 */
function render_wd_statement($stmt, &$conceptual_value) {
    $rank = array('deprecated'=>'↓ ', 'normal'=>': ', 'preferred'=>'↑ ');
    $pid = $stmt->mainsnak->property;
    $label = get_wd_property($pid);
    if ($pid == P_IMAGE) $text = '';
    else $text = "<a href='".URL_WDO."/wiki/Property:$pid' title='$label'>$pid</a>".$rank[$stmt->rank];
    $text .= ' '.render_snak($stmt->mainsnak, $conceptual_value);
    if (property_exists($stmt, 'qualifiers')) {
        $text .= '<small>';
        foreach (get_object_vars($stmt->qualifiers) as $snak_arr) {
            $indent = '<br><span class=indent>&nbsp;</span>';
            foreach($snak_arr as $snak) {
                $label = get_wd_property($snak->property);
                $text .= "$indent <a href='".URL_WDO."/wiki/Property:{$snak->property}'>$label</a> &mdash; ".render_snak($snak, $cv);
            }
        }
        $text .= '</small>';
    }
    if (property_exists($stmt, 'references')) {
        foreach ($stmt->references as $ref) {
            $text .= '<br><small><span class=indent>REF:</span> ';
            $indent = '';
            foreach (get_object_vars($ref->snaks) as $snak_arr) {
                foreach ($snak_arr as $snak) {
                    $label = get_wd_property($snak->property);
                    $text .= "$indent <a href='".URL_WDO."wiki/Property:{$snak->property}'>$label</a> &mdash; ".render_snak($snak, $cv);
                    $indent = '<br><span class=indent>&nbsp;</span>';
                }
            }
            $text .= '</small>';
        }
    }
    return $text;
}

/**
 * Render SNAK value into HTML
 *
 * @param $snak                 — SNAK representation as stdClass instance from deserialized json
 * @param $conceptual_value
 * @return text to display SNAK value in HTML code
 */
function render_snak($snak, &$conceptual_value) {
    global $WD_TIME_PRECISION, $WD_URLS;
    switch ($snak->snaktype) {
        case 'value':
            $val = $snak->datavalue;
            $v = $val->value;
            $conceptual_value = $v;
            switch ($val->type) {
                case 'number':
                case 'string':
                    $i = get_image($v);
                    if ($snak->datatype == 'commonsMedia') return "<center><a href='".URL_COM."wiki/File:".rawurlencode($v)."'><img src='$i[thumb]'><br>$v</a></center>";
                    if ($snak->datatype == 'url') return sprintf($WD_URLS['sitelinks'], $v, mb_strlen($v)>40 ? mb_substr($v, 0, 40).'...' : $v);
                    if (isset($WD_URLS[$snak->property])) return sprintf($WD_URLS[$snak->property], $v, $v);
                    return $v;
                case 'wikibase-entityid':
                    $conceptual_value = $v->id;
                    $label = get_wd_label($v->id);
                    return "<a href='".URL_WDO."wiki/{$v->id}'>$label <small>({$v->id})</small></a>";
                case 'time':
                    $d = date_parse($v->time);
                    $conceptual_value = $d['year'];
                    switch ($v->precision) {
                        case 5: /* 10 тыс лет */
                        case 6: /* 1000 лет */
                            return ($d['year']/1000).'&pm;'.$WD_TIME_PRECISION[$v->precision];
                        case 7: /* 100 лет */
                        case 8: /* 10 лет */
                        case 9: /* 1 год */
                            return $d['year'].'&pm;'.$WD_TIME_PRECISION[$v->precision];
                        case 10: /* 1 мес. */
                        case 11: /* 1 день */
                            return sprintf('%02d.%02d.%04d&pm;%s', $d['day'], $d['month'], $d['year'], $WD_TIME_PRECISION[$v->precision]);
                        default: 
                            return $v->time.'&pm;'.$WD_TIME_PRECISION[$v->precision];
                    }
                case 'monolingualtext':
                    $conceptual_value = $v->text;
                    return "[{$v->language}] {$v->text}";
                case 'globecoordinate':
                    return "
                        <table style='display: inline-block' border=0 cellpadding=0 cellspacing=0><tr><td rowspan=2 style='padding:0'><a href='https://tools.wmflabs.org/geohack/geohack.php?params={$v->latitude}_N_{$v->longitude}_E_globe:earth&language=ru' title='Карты и инструменты GeoHack'>{$v->latitude}, {$v->longitude}</a> &nbsp;</td>
                        <td style='padding:0'><font size='-2'><a href='https://maps.yandex.ru/?ll={$v->longitude},{$v->latitude}&spn=0.02,0.02&pt={$v->longitude},{$v->latitude}&l=map' title='Это место на Яндекс.Картах'>(Я)</a></font> &nbsp;
                        <font size='-2'><a href='https://maps.google.com/maps?ll={$v->latitude},{$v->longitude}&spn=0.02,0.02&t=m&q={$v->latitude},{$v->longitude}' title='Это место на картах Google'>(G)</a></font> &nbsp;
                        <font size='-2'><a href='https://www.openstreetmap.org/?mlat={$v->latitude}&mlon={$v->longitude}&zoom=13&layers=M' title='Это место на карте OpenStreetMap'>(O)</a></font></td></tr>
                        <tr><td style='padding:0; font-size:x-small'>&pm;{$v->precision}</td></tr></table>";
                default:
                    return $val->type.':'.json_encode($v);
            }
        default:
            $conceptual_value = $snak->snaktype;
            return $snak->snaktype;
    }
}

/**
 * Load property label or retrieve it from cache
 *
 * @param $id                   — property id
 * @return 'en' label of property from Wikidata or cache
 */
function get_wd_property($id) {
    global $propLookup, $cache;
    $item = $cache->getItem(A.$id);
    if ($item->isHit()) {
        return $item->get();
    }
    $p = $propLookup->getPropertyForId(new PropertyId($id));
    $label = $p->getLabels()->getByLanguage('en')->getText();
    $item->set($label);
    $item->expiresAfter(86400);
    $cache->save($item);
    return $label;
}

/**
 * Load entity label or retrieve it from cache
 *
 * @param $id                   — entity id
 * @return 'en'/'ru' label of entity from Wikidata or cache
 */
function get_wd_label($id) {
    global $termLookup, $cache;
    $item = $cache->getItem(A.$id);
    if ($item->isHit()) {
        return $item->get();
    }
    $label = $termLookup->getLabels(ItemId::newFromNumber(substr($id,1)), ['en', 'ru']);
    $label = reset($label);
    $item->set($label);
    $item->expiresAfter(600);
    $cache->save($item);
    return $label;
}

/**
 * Load wikidata entity
 *
 * @param $id                   — entity id
 * @return stdClass instance with deserialized json data
 */
function get_wd_entity($id) {
    $ch = curl_init(URL_WDO.'wiki/Special:EntityData/'.$id.'.json');
    curl_setopt($ch, CURLOPT_USERAGENT, "AVSBot/1.0");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $json = curl_exec($ch);
    $result=json_decode($json);
    return $result->entities->{$id};
}

/**
 * Calculate orthodromic distance between two WGS-84 points
 *
 * @param $p1_lat               — point 1, latitude (degrees)
 * @param $p1_long              — point 1, longitude (degrees)
 * @param $p2_lat               — point 2, latitude (degrees)
 * @param $p2_long              — point 2, longitude (degrees)
 * @return orthodromic distance (meters)
 */
function ortho_distance($p1_lat, $p1_long, $p2_lat, $p2_long) {
    return acos(sin($p1_lat/180*pi())*sin($p2_lat/180*pi())+cos($p1_lat/180*pi())*cos($p2_lat/180*pi())*cos(($p1_long-$p2_long)/180*pi()))*6371008;
}

/**
 * Get image properties from Commons or Wikivoyage 
 *
 * @param $image                — image name
 */
function get_image($image) {
    if (empty($image)) return false;
    $result = array('thumb' => false);
    foreach (array(URL_COM, URL_RWV) as $img_base_url) {
        $img_info = file_get_contents($img_base_url.'w/api.php?action=query&format=json&prop=imageinfo&titles=File:'.urlencode($image).'&iiprop=url&iiurlwidth=320&iiurlheight=240');
        $img_info = json_decode($img_info);
        $thumb = reset($img_info->query->pages);
        if (@property_exists($thumb, 'imageinfo')) {
            $result['thumb'] = $thumb->imageinfo[0]->thumburl;
            $result['img_base_url'] = $img_base_url;
            if ($result['thumb']) break;
        }
    }
    return $result;
}

/**
 * Collect references on official acts from Wikivoyage template data
 *
 * @param $wv                   — Wikivoyage template data
 * @return array of Wikidata SNAK instances
 */
function official_acts($wv) {
    $refs = array();
    foreach (array('document', 'document2', 'dismissed') as $tag) {
        if (!property_exists($wv, $tag) or empty($wv->$tag)) continue;
        $ref = array();
        $wv_document = parse_document($wv->$tag, $wv->region, $d);
        if (empty($wv_document)) continue;
        if ($d['wdid']>0) {
            $ref[] = new PropertyValueSnak($GLOBALS['P_STATED_IN'], new EntityIdValue(new ItemId('Q'.$d['wdid'])));
        } else {
            if (isset($d['url'])) {
                $ref[] = new PropertyValueSnak(isset($d['wm']) && $d['wm'] ? $GLOBALS['P_WM_URL'] : $GLOBALS['P_REF_URL'], new StringValue($d['url']));
            }
            $ref[] = new PropertyValueSnak($GLOBALS['P_TITLE'], new UnDeserializableValue(array('language'=>'ru','text'=>$d['title']), 'monolingualtext', ''));
        }
        $refs[] = $ref;
    }
    return $refs;
}

/**
 * Render legal act link based on Wikivoyage database 
 * 
 * @param $id                   — document id
 * @param $region               — region id
 * @param $row                  — row from database (output parameter)
 * @return HTML-link for the document
 */
function parse_document($id, $region, &$row) {
    global $dbh;
    $sth = $dbh->prepare("SELECT * FROM documents WHERE id = ? and region = ?");
    $sth->execute(array($id, $region));
    if ($sth->rowCount()>0) {
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        if (strpos($row['text'], '[http') === 0) {
            list($url, $text) = explode(' ', $row['text'], 2);
            $url = str_replace("'", '%27', mb_substr($url, 1));
            $text = mb_substr($text, 0, -1);
            if (parse_url($url) !== FALSE) {
                $row['url'] = $url;
                $row['title'] = $text;
            }
        } else if (strpos($row['text'], '[[') === 0) {
            list($url, $text) = explode('|', $row['text'], 2);
            $url = str_replace(' ', '_', mb_substr($url, $url{2} == ':' ? 3 : 2));
            $text = mb_substr($text, 0, -2);
            $row['url'] = URL_RWV.'wiki/'.rawurlencode($url);
            $row['wm'] = true;
            $row['title'] = $text;
        } else {
            $row['title'] = $row['text'];
            return $row['text'];
        }
        return "<a href='$row[url]'>".htmlspecialchars($text).'</a>';
    }
    if (!empty($region))
        return parse_document($id, '', $row);
    return '';
}

/**
 * Load monument data from Wikivoyage database and do some preliminary processing
 *
 * @param $id                   — knid (heritage identifier)
 * @param $row                  — row from sql result (output)
 * @return Wikivoyage template data
 */
function load_monument($id, &$row) {
    global $dbh;
    if (empty($id)) throw new Exception('Empty knid');
    $sth = $dbh->prepare("SELECT *,json, monuments.wdid AS mwd, pages.wdid AS pwd from monuments,pages WHERE knid = ? and monuments.pageid = pages.pageid");
    if ($sth->execute(array($id))) {
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        if ($sth->rowCount() != 1) $row = array();
    }
    $sth->closeCursor();
    if (empty($row)) return false;
    $wv = json_decode($row['json']);
    if (property_exists($wv, 'complex') and !empty($wv->complex)) {
        $sth = $dbh->prepare("SELECT wdid from monuments WHERE knid = ?");
        if ($sth->execute(array($wv->complex))) {
            $wv_complex_wdid = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($wv_complex_wdid['wdid']))
                $row['wv_complex_wdid'] = 'Q'.$wv_complex_wdid['wdid'];
        }
        $sth->closeCursor();
    }
    if (substr($id,2,1) == '4') $wv->protection = '4';
    if (empty($wv->protection) and !empty($wv->knid_new) and strlen($wv->knid_new) == 15) {
        switch ($wv->knid_new % 10) {
            case 4: $wv->protection = 'М'; break;
            case 5: $wv->protection = 'Р'; break;
            case 6: $wv->protection = 'Ф'; break;
        }
    }
    if (!property_exists($wv, 'commonscat')) $wv->commonscat = '';
    if (!empty($wv->commonscat)) $wv->commonscat = str_replace('_', ' ', $wv->commonscat);
    if (!property_exists($wv, 'wiki')) $wv->wiki = '';
    if (!empty($wv->wiki)) $wv->wiki = str_replace('_', ' ', $wv->wiki);
    $wv_address = array();
    if (property_exists($wv, 'block') and !empty($wv->block)) $wv_address[] = "квартал {$wv->block}";
    if (property_exists($wv, 'address') and !empty($wv->address)) $wv_address[] = $wv->address;
    $row['wv_address'] = implode(', ', $wv_address);
    return $wv;
}
