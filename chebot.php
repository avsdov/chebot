#!/usr/bin/php
<?php

/**
 * Export cultural heritage monuments data from Russian Wikivoyage database.
 * Usage:
 *      ./chebot.php 
 *
 * @author Avsolov
 *
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 */

empty($_SERVER['REQUEST_METHOD']) or die('Must be run from the shell!');

if ($argc<2) die('Missing command-line arguments!');

$ts_pw = posix_getpwuid(posix_getuid());
$ts_mycnf = parse_ini_file($ts_pw['dir']."/replica.my.cnf");
$dbh = new PDO('mysql:host=tools-db;dbname=s53506__ruheritage', $ts_mycnf['user'], $ts_mycnf['password']);
$AVSBOT = parse_ini_file('avsbot1.ini');
require_once($ts_pw['dir'].'/lib/vendor/autoload.php'); 
require_once('./wikidata_helper.php');
define('BLACKLIST_WD', $ts_pw['dir'].'/public_html/snow/blacklist_wd.txt');
define('BLACKLIST_WV', $ts_pw['dir'].'/public_html/snow/blacklist_wv.txt');
define('LASTLOG', $ts_pw['dir'].'/public_html/snow/last_chebot_log.txt');
ini_set('error_log', LASTLOG);
unset($ts_mycnf, $ts_pw);

$blacklist_wv = array_unique(file(BLACKLIST_WD, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES));
$blacklist_wd = array_unique(file(BLACKLIST_WV, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES));

use \Mediawiki\DataModel\EditInfo;
use \Wikibase\DataModel\Entity\EntityIdValue;
use \Wikibase\DataModel\Entity\ItemId;
use \Wikibase\DataModel\Reference;
use \Wikibase\DataModel\ReferenceList;
use \Wikibase\DataModel\Snak\PropertySomeValueSnak;
use \Wikibase\DataModel\Snak\PropertyValueSnak;
use \Wikibase\DataModel\Snak\SnakList;
use \DataValues\UnDeserializableValue;
use \DataValues\StringValue;

$api = new \Mediawiki\Api\MediawikiApi(URL_WDO.'w/api.php');
$api->login(new \Mediawiki\Api\ApiUser($AVSBOT['LOGIN'], $AVSBOT['PASSWORD']));
$wbFactory = get_wikidata_factory($api);
$termLookup = $wbFactory->newTermLookup();
$propLookup = $wbFactory->newPropertyLookup();

$wv_api = new \Mediawiki\Api\MediawikiApi(URL_RWV.'w/api.php');
$wv_api->login(new \Mediawiki\Api\ApiUser($AVSBOT['LOGIN'], $AVSBOT['PASSWORD']));

for ($idx=1; $idx<$argc; $idx++) {
    $PAGEID = sprintf('%d', $argv[$idx]);
    if (empty($PAGEID)) continue;
    my_log("== PAGE $PAGEID ==");
    
    $res = $wv_api->getRequest(new \Mediawiki\Api\SimpleRequest('query', array('pageids'=>$PAGEID,'prop'=>'revisions','rvprop'=>'timestamp')));
    $ts = $res['query']['pages'][$PAGEID]['revisions'][0]['timestamp'];
    $PAGE_CONTENT = file_get_contents(URL_RWV."w/index.php?curid=$PAGEID&action=raw");
    $md1 = md5($PAGE_CONTENT);

    $sth = $dbh->prepare("SELECT knid FROM monuments WHERE pageid = ? ORDER by knid");
    $sth->execute(array($PAGEID));
    $counter = 0;
    while ($knid = $sth->fetch(PDO::FETCH_ASSOC)) {
        my_log("update_entity($knid[knid])");
        try {
            if (update_entity($knid['knid'])) {
                $counter++;
                sleep(1);
//                if ($counter>1) break;
            }
        } catch (Exception $e) {
            my_log("\t...".$e->getMessage());
            error_log($e->getTraceAsString());
        }
    }

    $md2 = md5($PAGE_CONTENT);
    if ($md1 != $md2) {
        $res = $wv_api->postRequest(new \Mediawiki\Api\SimpleRequest('edit', 
            array('pageid'=>$PAGEID, 
                  'summary'=>"update wdid-s, #[[User:Avsolov/chebot|CHEBOT]]", 
                  'basetimestamp'=>$ts, 
                  'token'=>$wv_api->getToken(), 
                  'bot'=>true, 
                  'text'=>$PAGE_CONTENT)));
    }
}
my_log("=== all done ===");

function update_entity($id) {
    global $edit_info, $PAGE_CONTENT, $dbh, $wbFactory, $WD_TYPOLOGY, $WD_PROTECTION, $blacklist_wd, $blacklist_wv;
    if (in_array($id, $blacklist_wv)) {
        my_log("\t... blacklisted knid");
        return false;
    }
    $wv = load_monument($id, $row);
    if (empty($wv->wdid)) {
        if (!empty($wv->complex) and $wv->knid == $wv->complex and !empty($wv->commonscat)) {
        } else {
            if (empty($wv->lat) || empty($wv->long)) {
                my_log("\t... no coords — skipped");
                return false;
            }
            if (property_exists($wv, 'status') and $wv->status == 'destroyed') {
                my_log("\t... status=destroyed — skipped");
                return false;
            }
            if (!empty($wv->dismissed)) {
                my_log("\t... dismissed — skipped");
                return false;
            }
        }
        my_log("\t... create_wd_entity: ");
        $itemId = create_wd_entity($row, true);
        my_log("\t\t$itemId");
        $PAGE_CONTENT = set_wdid($PAGE_CONTENT, $id, $itemId);
        $wv->wdid = ''.$itemId;
        $sth = $dbh->prepare("UPDATE monuments SET wdid = ?, json = ? WHERE knid = ?");
        $sth->execute(array(substr($wv->wdid,1), json_encode($wv, JSON_UNESCAPED_UNICODE), $id));
    } else {
        $revision = $wbFactory->newRevisionGetter()->getFromId($wv->wdid);
        $wd_item = $revision->getContent()->getData();
        if (is_object($wd_item)) $wd = $wd_item->getStatements();
        else {
            my_log("\t... invalid wikidata entity — skipped");
            return false;
        }
        if (in_array($wv->wdid, $blacklist_wd)) {
            my_log("\t... blacklisted wdid");
            return false;
        }

        $edit_info = array();
        $refSnaks = array();
        $refSnaks[] = new PropertyValueSnak($GLOBALS['P_IMPORTED_FROM'], new EntityIdValue(new ItemId(Q_RUS_WIKIVOYAGE)));
        if (!empty($row['revid']))
            $refSnaks[] = new PropertyValueSnak($GLOBALS['P_WM_URL'], new StringValue(URL_RWV.'?oldid='.$row['revid'].'#'.$id));
        $knid_ref = array();
        if (!empty($row['pwd'])) {
            $knid_ref[] = new PropertyValueSnak($GLOBALS['P_STATED_IN'], new EntityIdValue(new ItemId('Q'.$row['pwd'])));
        }

        // label never checked again
        //    $wd_item->setLabel('ru', $wv->name);
        // $edit_info[] = 'label[ru]: '.$wv->name;

        // Set P571 (inception) if missing
        $wv_year = @($wv->year+0);
        if ($wv_year>500) {
            $p571 = $wd->getByPropertyId($GLOBALS['P_INCEPTION']);
            if ($p571->isEmpty()) {
                set_or_replace_value($wd, 
                    new UnDeserializableValue(array('time'=>"+$wv_year-00-00T00:00:00Z", 'timezone'=>0, 'before'=>0, 'after'=>0, 'precision'=>9, 'calendarmodel'=>'http://www.wikidata.org/entity/Q1985727'), 'time', ''),
                    $GLOBALS['P_INCEPTION'],
                    array(new Reference($refSnaks)));
            }
        }

        // P17 must exist also
        check_and_add_country_statement($id, $wd, $refSnaks, $wv_year);

        // image updated only if missing
        if (!empty($wv->image)) {
            $wv->image = str_replace('_', ' ', $wv->image);
            $img = get_image($wv->image);
            $p18 = $wd->getByPropertyId($GLOBALS['P_IMAGE']);
            if ($p18->isEmpty() and $img['img_base_url'] == URL_COM) {
                $wd->addNewStatement(new PropertyValueSnak($GLOBALS['P_IMAGE'], new StringValue($wv->image)), null, array(new Reference($refSnaks)));
                $edit_info[] = '[[d:Property:P18|image (P18)]]: '.$wv->image;
            }
        }

        // Set P1483 (KNID) if mismatch
        $p1483 = $wd->getByPropertyId($GLOBALS['P_KNID'])->toArray();
        $need_to_add = false;
        if (count($p1483) == 1) {
            if ($p1483[0]->getMainSnak()->getDataValue()->getValue() != $wv->knid) $need_to_add = true;
        } else $need_to_add = true;
        if ($need_to_add) {
            set_or_replace_value($wd, new StringValue($wv->knid), $GLOBALS['P_KNID'],
                empty($row['pwd']) ? null : array(new Reference($knid_ref)));
        }

        // Set P2817 (heritage list) if mismatch
        if (!empty($row['pwd'])) {
            $p2817 = $wd->getByPropertyId($GLOBALS['P_LIST'])->toArray();
            $need_to_add = false;
            if (count($p2817) == 1) {
                if ($p2817[0]->getMainSnak()->getDataValue()->getValue()['id'] != 'Q'.$row['pwd']) $need_to_add = true;
            } else $need_to_add = true;
            if ($need_to_add) {
                set_or_replace_value($wd, new EntityIdValue(new ItemId('Q'.$row['pwd'])), $GLOBALS['P_LIST'], null);
            }
        }

        // Set P2186 (WLM ID) if mismatch
        $p2186 = $wd->getByPropertyId($GLOBALS['P_WLM_ID'])->toArray();
        $wlmids = array('RU-'.$wv->knid);
        $wlmids_stmt = array($wv->knid =>
            array('prop' => new PropertyValueSnak($GLOBALS['P_WLM_ID'], new StringValue('RU-'.$wv->knid)),
                'qua' => null,
                'refs' => array(new PropertyValueSnak($GLOBALS['P_REF_URL'], new StringValue(URL_TFO.$wv->knid)))));
        if ((strncmp($wv->knid,"82",2)==0) || (strncmp($wv->knid,"92",2)==0)) {
            $wlmids_stmt[$wv->knid]['qua'] = new SnakList(array(new PropertyValueSnak($GLOBALS['P_OF'], $GLOBALS['Q_RUSSIA'])));
            if (property_exists($wv, 'uid') and !empty($wv->uid)) {
                $wlmids[] = 'UA-'.$wv->uid;
                $wlmids_stmt[$wv->uid] = array(
                    'prop' => new PropertyValueSnak($GLOBALS['P_WLM_ID'], new StringValue('UA-'.$wv->uid)),
                    'qua' => new SnakList(array(new PropertyValueSnak($GLOBALS['P_OF'], $GLOBALS['Q_UKRAINE']))),
                    'refs' => null);
            }
        }
        if (count($p2186) == count($wlmids)) {
            foreach ($p2186 as $stmt) {
                $idx = array_search($stmt->getMainSnak()->getDataValue()->getValue(), $wlmids);
                if ($idx !== false) unset($wlmids[$idx]);
            }
        }
        if (count($wlmids)>0) {
            foreach ($p2186 as $stmt) {
                $wd->removeStatementsWithGuid($stmt->getGuid());
            }
            foreach ($wlmids_stmt as $key => $stmt) {
                $wd->addNewStatement($stmt['prop'], $stmt['qua'], $stmt['refs'] ? array(new Reference($stmt['refs'])) : null);
                $edit_info[] = "[[:d:Property:".P_WLM_ID."|Wiki Loves Monuments ID (".P_WLM_ID.")]]: $key";
            }
        }

        // Set P361 (part of) if mismatch
        if (!empty($row['wv_complex_wdid']) and $row['wv_complex_wdid'] != $wv->wdid) {
            $q = new ItemId($row['wv_complex_wdid']);
            $p361 = $wd->getByPropertyId($GLOBALS['P_PART_OF']);
            $need_to_add = true;
            if (!$p361->isEmpty()) foreach ($p361 as $stmt) {
                if ($stmt->getMainSnak()->getDataValue()->getValue()['id'] == $row['wv_complex_wdid']) {
                    $need_to_add = false;
                    break;
                }
            }
            if ($need_to_add) {
                $wd->addNewStatement(new PropertyValueSnak($GLOBALS['P_PART_OF'], new EntityIdValue($q)), null, array(new Reference($refSnaks)));
                $edit_info[] = "[[:d:Property:P361|part of (P361)]]: [[:d:$row[wv_complex_wdid]|$row[wv_complex_wdid]]]";
            }
        }

        // Set P5381 (EGROKN) if mismatch
        if (!empty($wv->knid_new)) {
            $p5381 = $wd->getByPropertyId($GLOBALS['P_EGROKN'])->toArray();
            $need_to_add = false;
            if (count($p5381) == 1) {
                if ($p5381[0]->getMainSnak()->getDataValue()->getValue() != $wv->knid_new) $need_to_add = true;
            } else $need_to_add = true;
            if ($need_to_add) {
                set_or_replace_value($wd, new StringValue($wv->knid_new), $GLOBALS['P_EGROKN'], 
                    array(new Reference(array(new PropertyValueSnak($GLOBALS['P_STATED_IN'], new EntityIdValue(new ItemId(Q_EGROKN)))))));
            }
        }

        // Set P31 (instance of) with typology of cultural heritage monument
        $p31 = $wd->getByPropertyId($GLOBALS['P_INSTANCE_OF']);
        $need_to_update = false; $value_exists = false;
        if (!$p31->isEmpty()) foreach ($p31 as $stmt) {
            $q = $stmt->getMainSnak()->getDataValue()->getValue()['id'];
            if ($q == Q_RUS_HERITAGE) {
                $wd->removeStatementsWithGuid($stmt->getGuid());
                $need_to_update = true;
            }
            if ($q == $WD_TYPOLOGY[$wv->type]) $value_exists = true;
        }
        if (count($p31) == 1 and $value_exists) {
            if (strpos($wv->name, 'ерковь')) {
                $wd->addNewStatement(new PropertyValueSnak($GLOBALS['P_INSTANCE_OF'], new EntityIdValue(new ItemId(Q_CHURCH))), null, array(new Reference($refSnaks)));
                $edit_info[] = '[[:d:Property:P31|instance of (P31)]]: [[:d:'.Q_CHURCH.'|]]';
            } else if (strpos($wv->name, 'асовня')) {
                $wd->addNewStatement(new PropertyValueSnak($GLOBALS['P_INSTANCE_OF'], new EntityIdValue(new ItemId(Q_CHAPEL))), null, array(new Reference($refSnaks)));
                $edit_info[] = '[[:d:Property:P31|instance of (P31)]]: [[:d:'.Q_CHAPEL.'|]]';
            } else if (preg_match('|\b[Зз]дание\b|u', $wv->name) or preg_match('|\b[Дд]ом\b|u', $wv->name)) {
                $wd->addNewStatement(new PropertyValueSnak($GLOBALS['P_INSTANCE_OF'], new EntityIdValue(new ItemId(Q_BUILDING))), null, array(new Reference($refSnaks)));
                $edit_info[] = '[[:d:Property:P31|instance of (P31)]]: [[:d:'.Q_BUILDING.'|]]';
            }
        }
        if ($need_to_update and $value_exists) foreach ($p31 as $stmt) {
            $q = $stmt->getMainSnak()->getDataValue()->getValue()['id'];
            if ($q == $WD_TYPOLOGY[$wv->type]) {
                $wd->removeStatementsWithGuid($stmt->getGuid());
            }
        }
        if ($need_to_update or !$value_exists) {
            $wd->addNewStatement(new PropertyValueSnak($GLOBALS['P_INSTANCE_OF'], new EntityIdValue(new ItemId($WD_TYPOLOGY[$wv->type]))), null, array(new Reference($refSnaks)));
            $edit_info[] = '[[:d:Property:P31|instance of (P31)]]: [[:d:'.$WD_TYPOLOGY[$wv->type].'|'.$WD_TYPOLOGY[$wv->type].']]';
        }

        // Set P1435 (heritage designation) if mismatch
        $p1435 = $wd->getByPropertyId($GLOBALS['P_HERITAGE']);
        $refs = official_acts($wv);
        if (empty($refs) and $wv->knid{2} == '4' and !empty($row['pwd'])) {
            $refs = array($knid_ref);
        }
        $value_protection = @($WD_PROTECTION[$wv->protection]);
        if (empty($value_protection)) $value_protection = Q_RUS_HERITAGE;
        // process dismissed atribute
        $qua = null;
        if (property_exists($wv, 'dismissed')) {
            $snak = null;
            if (preg_match('|do?c?(\d{2})(\d{2})(\d{4})|', $wv->dismissed, $d)) {
                $snak = new PropertyValueSnak($GLOBALS['P_END_TIME'],
                    new UnDeserializableValue(array('time'=>"+$d[3]-$d[2]-$d[1]T00:00:00Z", 'timezone'=>0, 'before'=>0, 'after'=>0, 'precision'=>11, 'calendarmodel'=>'http://www.wikidata.org/entity/Q1985727'), 'time', ''));
            } else {
                $snak = new PropertySomeValueSnak($GLOBALS['P_END_TIME']);
            }
            $qua = new SnakList(array($snak));
        }
        $need_to_update = false; $value_exists = false;
        // Filter existsing statement
        if (!$p1435->isEmpty()) foreach ($p1435 as $stmt) {
            $value = $stmt->getMainSnak()->getDataValue()->getValue()['id'];
            if (!in_array($value, $WD_PROTECTION)) continue;
            if ($value != $value_protection) {
                $wd->removeStatementsWithGuid($stmt->getGuid());
                $need_to_update = true;
                continue;
            }
            if ($value == $value_protection) {
                if (!empty($refs) and $stmt->getReferences()->count() == 0) {
                    $wd->removeStatementsWithGuid($stmt->getGuid());
                    $need_to_update = true;
                    continue;
                }
                if ($qua != null and $stmt->getQualifiers()->isEmpty()) {
                    $wd->removeStatementsWithGuid($stmt->getGuid());
                    $need_to_update = true;
                    continue;
                }
                if ($value_exists) { /*duplicate*/
                    $wd->removeStatementsWithGuid($stmt->getGuid());
                    $need_to_update = true;
                    continue;
                }
                $value_exists = true;
            }
        }
        if ($need_to_update and $value_exists) foreach ($p1435 as $stmt) {
            $value = $stmt->getMainSnak()->getDataValue()->getValue()['id'];
            if ($value == $value_protection) {
                $wd->removeStatementsWithGuid($stmt->getGuid());
            }
        }
        if ($need_to_update or !$value_exists) {
            $wd->addNewStatement(
                new PropertyValueSnak($GLOBALS['P_HERITAGE'], new EntityIdValue(new ItemId($value_protection))),
                $qua,
                new ReferenceList(array_map(function ($val) {return new Reference($val);}, $refs)));
            $edit_info[] = "[[:d:Property:P1435|heritage designation (P1435)]]: [[:d:$value_protection|$value_protection]]";
        }

        // Set P131 (located in ATE) if mismatch (except settlements)
        if (!empty($wv->munid) and $wv->wdid != $wv->munid) {
            $p131 = $wd->getByPropertyId($GLOBALS['P_LOC_ATE'])->toArray();
            $need_to_add = true;
            foreach ($p131 as $stmt) {
                if ($stmt->getMainSnak()->getDataValue()->getValue()['id'] == $wv->munid) {$need_to_add = false; break;}
            }
            if ($need_to_add) {
                set_or_replace_value($wd, new EntityIdValue(new ItemId($wv->munid)), $GLOBALS['P_LOC_ATE'], array(new Reference($refSnaks)),
                    true /* keep qualified statements */);
            }
        }

        // Set directions if other location properties are missing
        while (!empty($row['wv_address'])) {
            $p6375 = $wd->getByPropertyId($GLOBALS['P_ADDRESS']);
            if (!$p6375->isEmpty()) break;
            $p669 = $wd->getByPropertyId($GLOBALS['P_STREET']);
            if (!$p669->isEmpty()) break;
            $p2795 = $wd->getByPropertyId($GLOBALS['P_DIRECTIONS']);
            $need_to_add = true;
            if (!$p2795->isEmpty()) foreach ($p2795 as $stmt) {
                $value = $stmt->getMainSnak()->getDataValue()->getValue();
                if ($value['language'] != 'ru') continue;
                if ($value['text'] == $row['wv_address']) {
                    $need_to_add = false;
                    break;
                }
                $wd->removeStatementsWithGuid($stmt->getGuid());
            }
            if ($need_to_add) {
                set_or_replace_value($wd,
                    new UnDeserializableValue(array('language'=>'ru','text'=>$row['wv_address']), 'monolingualtext', ''),
                    $GLOBALS['P_DIRECTIONS'],
                    array(new Reference($refSnaks)));
            }
            break;
        }

        // Export P625 (coordinates) if mismatch
        if (!empty($wv->lat) and !empty($wv->long)) {
            $p625 = $wd->getByPropertyId($GLOBALS['P_COORDINATES'])->toArray();
            $need_to_add = false;
            if (count($p625) == 1) {
                $value = $p625[0]->getMainSnak()->getDataValue()->getValue();
                if (ortho_distance($wv->lat, $wv->long, $value['latitude'], $value['longitude'])>30) $need_to_add = true;
            } else $need_to_add = (count($p625)==0);
            if ($need_to_add) {
                set_or_replace_value($wd, 
                    new UnDeserializableValue(array('latitude'=>(float)$wv->lat,'longitude'=>(float)$wv->long,'altitude'=>null,'precision'=>($wv->precise=='yes'?0.000001:0.0001),'globe'=>'http://www.wikidata.org/entity/Q2'), 'globecoordinate', ''),
                    $GLOBALS['P_COORDINATES'],
                    array(new Reference($refSnaks)));
            }
        }

        // Set P8316 (sobory) if mismatch
        if (!empty($wv->sobory)) {
            $p8316 = $wd->getByPropertyId($GLOBALS['P_SOBORY'])->toArray();
            $need_to_add = false;
            if (count($p8316) == 1) {
                if ($p8316[0]->getMainSnak()->getDataValue()->getValue() != $wv->sobory) $need_to_add = true;
            } else $need_to_add = true;
            if ($need_to_add) {
                set_or_replace_value($wd, new StringValue($wv->sobory), $GLOBALS['P_SOBORY'],
                    array(new Reference(array(new PropertyValueSnak($GLOBALS['P_STATED_IN'], new EntityIdValue(new ItemId(Q_SOBORY)))))));
            }
        }

        // Set P9343 (temples) if mismatch
        if (!empty($wv->temples)) {
            $p9343 = $wd->getByPropertyId($GLOBALS['P_TEMPLES'])->toArray();
            $need_to_add = false;
            if (count($p9343) == 1) {
                if ($p9343[0]->getMainSnak()->getDataValue()->getValue() != $wv->temples) $need_to_add = true;
            } else $need_to_add = true;
            if ($need_to_add) {
                set_or_replace_value($wd, new StringValue($wv->temples), $GLOBALS['P_TEMPLES'],
                    array(new Reference(array(new PropertyValueSnak($GLOBALS['P_STATED_IN'], new EntityIdValue(new ItemId(Q_TEMPLES)))))));
            }
        }

        // Set P576 (abolished...) if missing
        if (property_exists($wv, 'status') and $wv->status == 'destroyed') {
            $p576 = $wd->getByPropertyId($GLOBALS['P_ABOLISHED']);
            if ($p576->isEmpty()) {
                $wd->addNewStatement(new PropertySomeValueSnak($GLOBALS['P_ABOLISHED']), null, array(new Reference($refSnaks)));
                $edit_info[] = '[[:d:Property:P576|]]: somevalue';
            }
        }

        // Set commonswiki and P373 if missing
        $purge_commonswiki = false;
        if (!empty($wv->commonscat)) {
            $p = get_page_properties(URL_COM.'w/api.php', 'Category:'.$wv->commonscat);
            if (!empty($p)) {
                $p373 = $wd->getByPropertyId($GLOBALS['P_COMMONS_CAT']);
                if ($p373->isEmpty()) {
                    set_or_replace_value($wd, new StringValue($wv->commonscat), $GLOBALS['P_COMMONS_CAT'], array(new Reference($refSnaks)));
                }
                if (!$wd_item->hasLinkToSite('commonswiki')) {
                    if (empty($p->properties['wikibase_item'])) {
                        $wd_item->addSiteLink(new \Wikibase\DataModel\SiteLink('commonswiki', 'Category:'.$wv->commonscat));
                        $edit_info[] = "commonswiki: Category:{$wv->commonscat}";
                        $purge_commonswiki = true;
                    } else
                        my_log("\t...commonscat '{$wv->commonscat}' is already bound to ".$p->properties['wikibase_item']);
                }
            } else {
                my_log("\t...invalid commonswiki '{$wv->commonscat}'");
            }
        }

        // Set ruwiki if missing
        $purge_ruwiki = false;
        if (!empty($wv->wiki) and !$wd_item->hasLinkToSite('ruwiki')) {
            $p = get_page_properties(URL_RWP.'w/api.php', $wv->wiki);
            if (!empty($p)) {
                if (empty($p->properties['wikibase_item'])) {
                    $wd_item->addSiteLink(new \Wikibase\DataModel\SiteLink('ruwiki', $wv->wiki));
                    $edit_info[] = "ruwiki: {$wv->wiki}";
                    $purge_ruwiki = true;
                } else {
                    my_log("\t...wiki '{$wv->wiki}' is already bound to ".$p->properties['wikibase_item']);
                }
            } else {
                my_log("\t...invalid wiki '{$wv->wiki}'");
            }
        }

        if (count($edit_info)>0) {
            $edit_str = implode(', ', $edit_info);
            my_log("\t... $edit_str");
            $dots = '';
            while (strlen($edit_str)>400) {
                array_pop($edit_info);
                $edit_str = implode(', ', $edit_info);
                $dots = '..., ';
            }
            $edit_info[] = $dots.'#[[:voy:ru:User:Avsolov/chebot|CHEBOT]]';
            $edit_info = 'set '.implode(', ', $edit_info);
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo($edit_info, EditInfo::NOTMINOR, EditInfo::BOT, 5));
            if ($purge_commonswiki)
                purge_page_cache(URL_COM.'w/api.php', 'Category:'.$wv->commonscat);
            if ($purge_ruwiki)
                purge_page_cache(URL_RWP.'w/api.php', $wv->wiki);
        } else return false;
    }
    return true;
}

function my_log($message) {
    echo sprintf('%.3f',microtime(true)).' '.$message."\n";
    error_log($message);
}
